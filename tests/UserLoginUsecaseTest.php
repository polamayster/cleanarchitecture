<?php

namespace tests;

use Template\Core\AbstractGateway;
use Template\Core\AbstractOutput;
use Template\Core\AbstractTranslator;
use Template\Gateways\Datasources\MemoryDatasource;
use Template\Gateways\UserGateway;
use Template\Usecases\UserLoginUseCase;


class TranslatorStub extends AbstractTranslator {

    public function trans($string, $params = array()) {
        return strtr($string, $params);
    }
}


class OutputStub extends AbstractOutput {

    /**
     * @param array $data
     * @return mixed
     */
    public function execute($data = array()) {
        return $this->getErrors();
    }
}


class UserLoginUseCaseTest extends \PHPUnit_Framework_TestCase {

    protected $_userGatewayStub;
    protected $_output;
    protected $_translator;

    public function testValidLoginUsecase() {
        $input = array('username' => 'foo',
                       'password' => 'bar');

        $usecase = new UserLoginUseCase($input, $this->getUserGatewayStub(), $this->getOutput(), $this->getTranslator());
        $usecase->execute();

        $this->assertFalse($this->getOutput()->hasErrors());
    }

    public function testInValidLoginUsecase() {
        $input = array('username' => 'for',
                       'password' => 'invalid_bar');

        $usecase = new UserLoginUseCase($input, $this->getUserGatewayStub(), $this->getOutput(), $this->getTranslator());
        $usecase->execute();

        $this->assertTrue($this->getOutput()->hasErrors());
    }

    /**
     * @return AbstractOutput
     */
    public function getOutput() {
        return $this->_output;
    }

    /**
     * @return AbstractGateway
     */
    public function getUserGatewayStub() {
        return $this->_userGatewayStub;
    }

    /**
     * @return AbstractTranslator
     */
    public function getTranslator() {
        return $this->_translator;
    }

    /**
     * @return array
     */
    public function getFixtures() {
        return $this->_fixtures;
    }

    protected $_fixtures = array(
        array('id' => 1,
              'username' => 'foo',
              'password' => 'bar'),
        array('id' => 2,
              'username' => 'baz',
              'password' => 'zoo'),
    );

    protected function setUp() {
        $this->_init_fixtures();
        $this->_output = new OutputStub();
        $this->_translator = new TranslatorStub();
    }

    protected function _init_fixtures() {
        $datasource = new MemoryDatasource();
        $this->_userGatewayStub = new UserGateway($datasource);

        foreach ($this->getFixtures() as $document) {
            $user = $this->_userGatewayStub->getEntity();
            $user->hydrate($document);
            $this->_userGatewayStub->create($user);
        }
    }
}