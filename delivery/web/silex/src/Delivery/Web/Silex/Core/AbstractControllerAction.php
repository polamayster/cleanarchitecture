<?php

namespace Delivery\Web\Silex\Core;


use Delivery\Web\Silex\Adapters\Application;

abstract class AbstractControllerAction {

    /**
     * @param Application $app
     * @return mixed
     */
    abstract public function execute(Application $app);
} 