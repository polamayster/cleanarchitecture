<?php

namespace Delivery\Web\Silex\Controllers\Actions;


use Delivery\Web\Silex\Core\AbstractControllerAction;
use Delivery\Web\Silex\Adapters\Application;
use Symfony\Component\HttpFoundation\Request;

class ShowLoginPageControllerAction extends AbstractControllerAction {

    /**
     * @param Application $app
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function execute(Application $app) {

        /** @var Request $request */
        $request = $app['request'];
        $app['locale'] = $request->query->get('_locale', 'en');

        return $app->renderView('login.twig');
    }
}
