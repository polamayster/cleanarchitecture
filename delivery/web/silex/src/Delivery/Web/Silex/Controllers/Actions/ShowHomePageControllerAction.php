<?php

namespace Delivery\Web\Silex\Controllers\Actions;


use Delivery\Web\Silex\Adapters\Application;
use Delivery\Web\Silex\Core\AbstractControllerAction;

class ShowHomePageControllerAction extends AbstractControllerAction {

    /**
     * @param Application $app
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function execute(Application $app) {
        return $app->redirect('/login');
    }

} 