<?php

namespace Delivery\Web\Silex\Adapters;


use Silex\Translator as SilexTranslator;
use Template\Core\AbstractTranslator;

class Translator extends AbstractTranslator {

    protected $_translator;

    /**
     * @param SilexTranslator $_translator
     */
    function __construct(SilexTranslator $_translator) {
        $this->_translator = $_translator;
    }

    /**
     * @return \Silex\Translator
     */
    public function getTranslator() {
        return $this->_translator;
    }

    /**
     * @param string $string
     * @param array $params
     * @return string
     */
    public function trans($string, $params = array()) {
        return $this->getTranslator()->trans($string, $params);
    }

} 