<?php

namespace Delivery\Web\Silex\Outputs;


use Symfony\Component\HttpFoundation\Request;
use Template\Core\AbstractOutput;

class LoginPageOutput extends AbstractOutput {

    protected $_twig;

    /**
     * @param \Twig_Environment $twig
     * @param Request $request
     */
    function __construct(\Twig_Environment $twig, Request $request) {
        $this->_twig = $twig;
        $this->_request = $request;
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Request
     */
    public function getRequest() {
        return $this->_request;
    }

    /**
     * @return \Twig_Environment
     */
    public function getTwig() {
        return $this->_twig;
    }

    /**
     * @param array $data
     * @return mixed
     */
    public  function execute($data = array()) {

        if ($this->hasErrors()) {
            return $this->getTwig()->render('login.twig', array('errors' => $this->getErrors(),
                                                                'username' => $this->getRequest()->request->get('username', null)));
        }

        return $this->getTwig()->render('secure.twig');
    }
}