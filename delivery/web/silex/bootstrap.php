<?php

require_once('configs/defines.php');

use Delivery\Web\Silex\Adapters\Application;
use Silex\Translator;
use Symfony\Component\Translation\Loader\YamlFileLoader;

$app = new Application();

$app['debug'] = true;
$app['locale'] = 'en';

$app->register(new Silex\Provider\SessionServiceProvider());
$app->register(new Silex\Provider\ValidatorServiceProvider());
$app->register(new Silex\Provider\UrlGeneratorServiceProvider());

$app->register(new Silex\Provider\TranslationServiceProvider(), array(
    'locale_fallback' => 'en',
));

$app['translator'] = $app->share($app->extend('translator', function($translator, $app) {
    /** @var Translator $translator */
    $translator->addLoader('yaml', new YamlFileLoader());
    $translator->addResource('yaml', __LOCALES_DIR__ . '/en.yml', 'en');
    $translator->addResource('yaml', __LOCALES_DIR__ . '/es.yml', 'es');

    return $translator;
}));

$app->register(new Silex\Provider\MonologServiceProvider(), array(
    'monolog.logfile'       => __RUNTIME_DIR__ . '/web/application.log',
    'monolog.class_path'    => __VENDOR_DIR__ . '/monolog/src',
));

$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.options'    => array('cache' => ($app['debug']) ? false :__RUNTIME_DIR__ . '/web/twig',
        'strict_variables' => true),
    'twig.path'       => __TEMPLATES_DIR__,
    'twig.class_path' => array(__VENDOR_DIR__ . '/twig/lib',
        __VENDOR_DIR__ . '/twig-extentions/lib'),
));

return $app;