<?php

namespace Template\Validators;

use Template\Core\AbstractValidationRule;


class ValidationRuleList {

    protected $_rules = array();

    /**
     * @param AbstractValidationRule $rule
     * @param $value
     * @param string $context
     */
    public function addRule(AbstractValidationRule $rule, $value, $context="default") {
        $this->_rules[] = array('rule' => $rule, 'value' => $value, 'context' => $context);
    }

    /**
     * @param array $rules
     */
    public function addRules(Array $rules) {

        foreach ($rules as $rule) {
            $context = (!array_key_exists('context', $rule)) ? 'default' : $rule['context'];
            $this->addRule($rule['rule'], $rule['value'], $context);
        }
    }

    /**
     * @return ValidationResult
     */
    public function apply() {
        $listValidationResult = new ValidationResult();

        foreach ($this->_rules as $ruleContainer) {
            /* @var $rule AbstractValidationRule */
            $rule = $ruleContainer['rule'];
            $value = $ruleContainer['value'];
            $context = $ruleContainer['context'];
            $ruleValidationResult = $rule->apply($value, $context);
            $listValidationResult->addResult($ruleValidationResult);

            if ($listValidationResult->hasErrors() && $rule->isCritical()) {
                break;
            }
        }

        return $listValidationResult;
    }
}
