<?php

namespace Template\Core;


use Template\Validators\ValidationResult;

abstract class AbstractOutput {

    protected $_errors = array();

    public function addError($message, $context="default") {
        $this->_errors[] = array($message => $context);
    }

    public function addValidationResult(ValidationResult $validationResult) {

        foreach ($validationResult->getErrors() as $errorContainer) {

            foreach ($errorContainer as $message => $context) {
                $this->addError($message, $context);
            }
        }
    }

    /**
     * @return boolean
     */
    public function hasErrors() {
        return count($this->_errors) > 0;
    }

    /**
     * @return array
     */
    public function getErrors() {
        return $this->_errors;
    }

    /**
     * @param array $data
     * @return mixed
     */
    abstract public function execute($data=array());
} 