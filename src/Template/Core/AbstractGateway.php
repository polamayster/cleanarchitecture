<?php

namespace Template\Core;


abstract class AbstractGateway {

    protected $_datasource;

    /**
     * @param AbstractDatasource $_datasource
     */
    function __construct(AbstractDatasource $_datasource) {
        $this->_datasource = $_datasource;
    }

    /**
     * @param AbstractEntity $entity
     * @return $this
     */
    public function create(AbstractEntity $entity) {
        return $entity->hydrate($this->_datasource->create($entity->dehydrate()));
    }

    /**
     * @param $value
     * @param null $key
     * @return $this|null
     */
    public function read($value, $key=null) {
        $document = $this->_datasource->read($value, $key);

        if (!is_null($document)) {
            return $this->getEntity()->hydrate($document);
        }

        return null;
    }

    /**
     * @codeCoverageIgnore
     */
    public function update(AbstractEntity $entity) {
        // TODO: Implement update() method.
    }

    /**
     * @codeCoverageIgnore
     */
    public function delete(Array $conditions) {
        // TODO: Implement delete() method.
    }

    /**
     * @codeCoverageIgnore
     */
    public function readAll(Array $conditions) {
        // TODO: Implement readAll() method.
    }

    /**
     * @codeCoverageIgnore
     */
    public function getUnitname() {
        // TODO: Implement getUnitname() method.
    }

    /**
     * @codeCoverageIgnore
     */
    public function drop() {
        // TODO: Implement drop() method.
    }

    /**
     * @codeCoverageIgnore
     */
    public function truncate() {
        // TODO: Implement truncate() method.
    }

    /**
     * @return \Template\Core\AbstractEntity
     */
    abstract public function getEntity();
}

