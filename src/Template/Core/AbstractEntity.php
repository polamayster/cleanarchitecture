<?php

namespace Template\Core;


abstract class AbstractEntity {

    /**
     * @param array $data
     * @return $this
     */
    public function hydrate(Array $data) {
        foreach($data as $key => $value) {
            $this->$key = $value;
        }

        return $this;
    }

    /**
     * @return array
     */
    public function dehydrate() {
        $result = array();

        foreach((array) $this as $key => $value) {
            $result[$key] = $value;
        }

        return $result;
    }
}