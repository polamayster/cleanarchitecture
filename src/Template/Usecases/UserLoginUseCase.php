<?php

namespace Template\Usecases;


use Template\Core\AbstractGateway;
use Template\Core\AbstractOutput;
use Template\Core\AbstractTranslator;
use Template\Core\AbstractUsecase;
use Template\Validators\Rules\KeyInArrayAndNotEmptyValidationRule;
use Template\Validators\Rules\UserCredentialsValidationRule;
use Template\Validators\ValidationResult;
use Template\Validators\ValidationRuleList;

class UserLoginUseCase extends AbstractUsecase {

    protected $_input;
    protected $_gateway;

    /**
     * @param array $input
     * @param AbstractGateway $gateway
     * @param AbstractOutput $output
     * @param AbstractTranslator $translator
     */
    public function __construct(Array $input,
                                AbstractGateway $gateway,
                                AbstractOutput $output,
                                AbstractTranslator $translator)
    {
        $this->_input = $input;
        $this->_gateway = $gateway;
        $this->_output = $output;
        $this->_translator = $translator;
    }

    /**
     * @return mixed
     */
    public function execute() {
        $validationResult = $this->_validateInput();
        $this->_output->addValidationResult($validationResult);

        return $this->_output->execute();
    }

    /**
     * @return ValidationResult
     */
    protected function _validateInput() {
        $validationRuleList = new ValidationRuleList();
        $input = $this->_input;

        $validationRuleList->addRule(new KeyInArrayAndNotEmptyValidationRule(
            $input,
            true,
            $this->_translator->trans("validation.username_should_be_specified")
        ), 'username', 'username');

        $validationRuleList->addRule(new KeyInArrayAndNotEmptyValidationRule(
            $input,
            true,
            $this->_translator->trans("validation.password_should_be_specified")
        ), 'password', 'password');

        $validationRuleList->addRule(new UserCredentialsValidationRule(
            $this->_gateway,
            true,
            $this->_translator->trans("validation.invalid_credentials_were_given")
        ), $input, 'password');

        $validationRuleListResult = $validationRuleList->apply();

        return $validationRuleListResult;
    }
}